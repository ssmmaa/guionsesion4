[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Guión de Prácticas
## 4. Ontología de comunicación entre agentes

En este guión se muestran las utilidades que nos proporciona la [biblioteca](https://jade.tilab.com/doc/api/jade/content/package-frame.html) de agentes Jade para poder generar el contenido de los mensajes [ACL](https://jade.tilab.com/doc/api/jade/lang/acl/package-frame.html) mediante la definición de una **ontología** para esta sesión de prácticas. Para definir nuestra ontología partimos de la implementación ya presentada en el [Guión de la Sesión 3](https://gitlab.com/ssmmaa/guionsesion3), es decir, utilizaremos los mismos agentes y tareas que ya están implementadas y solo realizaremos las modificaciones que nos permitirán utilizar la ontología que se define en este guión.

## 4.1 Ontologías en JADE

Con la utilización de las utilidades que nos proporciona la **biblioteca Jade**  para el uso de ontologías podremos generar el contenido de los mensajes **ACL** mediante elementos de información de la **ontología** y transformar el contenido de un mensaje **ACL** en los elementos de información de la **ontología**. Esto se muestra en la siguiente imagen:

![][imagen1]

El elemento que nos permite realizar esas operaciones es la clase [`ContentManager`](https://jade.tilab.com/doc/api/jade/content/ContentManager.html). Los métodos que tendremos que utilizar en nuestro código serán los siguientes:

- `registerLanguage(Codec c)` : Nos permite establecer el lenguaje que se utilizará para el contenido del mensaje. Nosotros utilizaremos el lenguaje **SL**, *Semantic Lenguaje*,  que está implementada en la biblioteca mediante la clase [`SLCodec`](https://jade.tilab.com/doc/api/jade/content/lang/sl/SLCodec.html).
- `registerOntology(Ontology o)`: Se enlaza la referencia de la ontología para poder realizar las transformaciones entre los elementos de ontología y el contenido del mensaje y viceversa.
- `fillContent(ACLMessage msg, ContentElement contenido)`: Este método permite transformar el elemento de información de la ontología representado por un objeto de [`ContentElement`](https://jade.tilab.com/doc/api/jade/content/ContentElement.html), se presentarán más adelante, en el contenido que tendrá el mensaje que se pasa como parámetro a este método.
- `ContentElement  extractContent(ACLMessage msg)`: Este es el método contrario al anterior. Se recupera el elemento de información de la ontología contenido en el mensaje.

Todos los agentes de la plataforma tienen asociado un objeto `ContentManager` y para obtener la referencia utilizaremos el método `getContentManager()` del agente. 

Para realizar las transformaciones la biblioteca Jade nos proporciona las definiciones presentes en el paquete [jade.content](https://jade.tilab.com/doc/api/jade/content/package-frame.html). En la siguiente imagen se presenta la jerarquía de los elementos que deberemos utilizar para el uso de las ontologías con nuestros agentes Jade.

![][imagen2]

En la imagen se muestra el `ContentElement` que es el elemento que se transformará en el contenido del mensaje **ACL** y el elemento de obtendremos del contenido del mensaje **ACL**. También se pueden apreciar unos elementos en *negrita* que utilizaremos para la definición de las ontologías que utilizaremos en las prácticas de la asignatura. 

En nuestros agente crearemos una variables de instancia que nos permitirán realizar las tareas de transformación descritas de forma cómoda.

```java
...
// Para trabajar con la ontología
private final ContentManager manager = getContentManager();
	
// El lenguaje utilizado por el agente para la comunicación es SL 
private final Codec codec = new SLCodec();

// La ontología que utilizará el agente
private Ontology ontology;
...
```

En el método `setup()` del agente hay que obtener la referencia de la ontología que utiliza el agente y configurar el `ContentManager` para las transformaciones.

```java
...
//Registro de la Ontología
try {
    ontology = OntoSesion4.getInstance();
} catch (BeanOntologyException ex) {
    Logger.getLogger(AgenteConsola.class.getName()).log(Level.SEVERE, null, ex);
}
manager.registerLanguage(codec);
manager.registerOntology(ontology);
...
```

Para el ejemplo del guión se realiza un tratamiento por defecto si se produce alguna excepción. Para las prácticas hay que dar una respuesta apropiada a las excepciones que se puedan producir con el uso de las ontologías por parte del agente. En este caso sería conveniente finalizar el agente ya que se ha producido un error con la ontología y no podrá comunicarse apropiadamente con otros agentes en la plataforma.

## 4.2 Definición de la ontología

En este guión solo se presenta una definición muy básica de la ontología que utilizaremos ya que el objetivo del guión es presentar los elementos necesarios para el uso de la ontología en el intercambio de mensajes entre los agentes. En el proyecto [OntoJuegoTablero](https://gitlab.com/ssmmaa/curso2019-20/ontojuegotablero) se presentará una documentación más completa para el análisis, diseño e implementación de ontologías para su uso con agentes Jade.

Para la ontología del guión solo vamos a definir los siguientes conceptos:

![][imagen3]

 - `Operando` : Este será un concepto abstracto que representa los elementos básicos que compondrán las operaciones que pueden realizar nuestros agentes. Será necesario para una futura ampliación de nuestra ontología si deseamos añadir nuevas operaciones. 
 - `Punto2D`: Hereda de `Operando` y define el elemento que hemos estado utilizando en los guiones precedentes. Tendrá las siguientes características:
	 - `x` : un número que representará la coordenada x del punto.
	 - `y` : un número que representará la coordenada y del punto.

Como solo vamos a mantener la operatividad ya implementada en el guión de la Sesión 3 no serán necesarios más conceptos para representar la información que necesita el `AgenteOperacion`. 

El siguiente elemento de la ontología serán las acciones que deben realizar los agentes:

![][imagen4]

- `SolicitarOperacion` : Representa la información que recibe el `AgenteOperacion` para completar una `Operacion` solicitada.
	- `operacion` : es la operación que tendrá que completar.
	- `operandos` : es una colección con los datos necesarios para la operación. Esta colección contiene elementos de `Operando`.
- `SolicitarSubscripcion` : Permite representar la información necesaria para realizar una subscripción.
	- `agente` : es el agente que solicita la subscripción.
	- `servicio` : es la identificación del servicio para la subscripción. 

Las respuestas que deben dar nuestros agentes están representados por los siguientes elementos de la ontología:

![][imagen5]

- `Respuesta` : Cada vez que un agente debe comunicar alguna incidencia a las peticiones que recibe utilizará este elemento de la ontología.
	- `servicio` : identifica el servicio que realiza el agente que responde.
	- `justificacion` : el motivo que justifica la respuesta que está realizando el agente.
- `Resultado` : A la petición que recibe el AgenteOperación responderá con este elemento de la ontología que representará el resultado de la acción que se le ha solicitado.
	- `servicio` : identifica el servicio que realiza el agente que responde.
	- `contenido` : será el resultado de la acción solicitada al agente.
- `MensajeConsola` : Representa la información que se presentará al usuario en una ventana gráfica que cumple con la utilidad de una consola para un agente.
	- `nombreAgente` : almacena el nombre del agente para que se le asocie la ventana donde se presentarán sus mensajes.
	- `contenido` : mensaje que se presentará en la ventana que representa la consola del agente.

Los atributos que forman parte de los elementos de la ontología deberán estar restringidos a un vocabulario establecido. En nuestro caso utilizaremos los siguientes elementos de vocabulario:

- `TipoServicio` : representará los tipos de servicio que pueden tener los agentes que utilizan la ontología.
- `NombreServicio` : identifica un servicio que puede proporcionar un agente que utilice la ontología.
- `Operacion` : representa las operaciones que podrán ser proporcionadas por los agentes que utilicen la ontología.
- `Incidencia` : representa las posibilidades de respuesta que tendrán los agentes, que utilizan la ontología, a las diferentes acciones que se le soliciten.

Los atributos de los elementos de la ontología serán obligatorios, es decir, ningún elemento de la ontología se considera correctamente construido si no está completo. Además, en el caso de `SolicitarOperación`, el atributo `operandos` será una colección de elementos de `Operando` y al menos contendrá un elemento.

## 4.4 Diseño de la ontología

Las diferentes clases que conformarán los elementos de nuestra ontología deben implementar una de las siguientes interfaces que se encuentran en el paquete [jade.content](https://jade.tilab.com/doc/api/jade/content/package-frame.html):

- `Concept` : Los elementos de información que conforman nuestra ontología estarán representados por clases que implementan esta interface. No son elementos que formarán directamente el contenido de los mensajes **ACL**.
- `AgentAction` : Es una interface que especializa el elemento anterior pero también es una especialización de `ContentElement`. Todas las acciones que los **agentes iniciadores** pidan a los **agentes participantes** estarán representadas por este elemento de ontología. Estos elementos de ontología si formarán parte del contenido de los mensajes **ACL**, aunque no directamente si no formando parte de un objeto [`Action`](https://jade.tilab.com/doc/api/jade/content/onto/basic/Action.html).
- `Predicate` : También es una especialización de `ContentElement` y su principal finalidad es intercambiar las afirmaciones que los agentes se intercambian entre ellos para añadir a su conocimiento. En nuestro caso, al tratarse de agentes reactivos, los utilizamos para contestar a los estímulos que reciben los diferentes agentes. Los estímulos los representaremos como `AgentAction`.

Para el diseño de la ontología lo primero que definiremos es un paquete donde se incluirá la clase que represente la ontología y una interface que recoja el vocabulario de la misma. En el ejemplo creamos el paquete `es.uja.ssmmaa.guionsesion4.ontologia`:

- `OntoSesion4` : Será la clase que representa nuestra ontología e implementa el patrón *singleton*.

```java
public class OntoSesion4 extends BeanOntology {
    private static final long serialVersionUID = 1L;
    
    // NOMBRE
    public static final String ONTOLOGY_NAME = "Ontologia_Sesion4";
    
    // The singleton instance of this ontology
    private static Ontology INSTANCE;

    public synchronized final static Ontology getInstance() throws BeanOntologyException {
        
        if (INSTANCE == null) {
            INSTANCE = new OntoSesion4();
	}
            
        return INSTANCE;
    }

    /**
     * Constructor
     * 
     * @throws BeanOntologyException
     */
    private OntoSesion4() throws BeanOntologyException {
	
        super(ONTOLOGY_NAME);
        
        add("es.uja.ssmmaa.guionsesion4.ontologia.elm");
    }
}
```

Nuestra ontología debe heredar de la clase `BeanOntology`[^nota1] que nos permite definir los deferentes elementos que la forman como las clases que se encuentran en el paquete  `es.uja.ssmmaa.guionsesion4.ontologia.elm`. De esta forma será muy cómodo añadir o modificar los elementos de forma muy cómoda y sencilla. Es conveniente leer el tutorial[^nota1] para completar la documentación para el desarrollo de ontologías en Jade.

- `Vocabulario` : En esta interface incluiremos todas las definiciones necesarias que conformarán los valores que pueden tener los atributos de los elementos de la ontología. También añadiremos los valores que representarán los servicios de nuestros agentes para la subscripción en el servicio de páginas amarillas para los agentes que utilizarán esta ontología.

```java
public interface Vocabulario {
    public enum TipoServicio {
        GUI, UTILIDAD, SISTEMA; 
    }
    public static final TipoServicio[] TIPOS = TipoServicio.values();
    public enum NombreServicio {
        OPERACION, CONSOLA, FORMULARIO;
    }
    public static final NombreServicio[] SERVICIOS = NombreServicio.values();
    
    public enum Operacion {
        SUMA, RESTA, MULTIPLICACION, DIVISION;
    }
    public static final Operacion[] OPERACIONES = Operacion.values();
    
    public static enum Incidencia {
        CANCELACION, ERROR_CANCELACION, ERROR_AGENTE, ERROR_MENSAJE_INCORRECTO, ERROR_CONTENIDO_MENSAJE,
        ONTOLOGIA_DESCONOCIDA, OPERACION_NO_DEFINIDA, OPERACIONES_SUPERADAS, OPERACION_ACEPTADA,
        SUBSCRIPCION_CANCELADA, SUBSCRIPCION_CREADA, ERROR_REGISTRO_SUBSCRIPCION
    } 
}
```

### 4.4.1 Elementos de la ontología

Todos los elementos que conforman la ontología los recogeremos en el paquete `es.uja.ssmmaa.guionsesion4.ontologia.elm` y serán clases que implementarán una de las interfaces que se han presentado anteriormente. Presentaré uno de los elementos para destacar las características más relevantes que todos los elementos comparten:

```java
public class RealizarOperacion implements AgentAction {
    private Operacion operacion;
    private List operandos;

    public RealizarOperacion() {
        this.operacion = null;
        this.operandos = null;
    }

    public RealizarOperacion(Operacion operacion, List operandos) {
        this.operacion = operacion;
        this.operandos = operandos;
    }

    @Slot(mandatory=true)
    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    @AggregateSlot(cardMin=1, type=Operando.class)
    public List getOperandos() {
        return operandos;
    }

    public void setOperandos(List operandos) {
        this.operandos = operandos;
    }

    @Override
    public String toString() {
        return "RealizarOperacion{" + "operacion=" + operacion + ", operandos=" + operandos + '}';
    }
}
```

Podemos observar que es como una clase normal de Java, pero solo nos centramos en atributos que la componen. Es decir, solo necesitaremos los métodos de acceso de esos atributos. No se implementarán más métodos porque no lo trataremos como una clase normal de datos de nuestros agentes. También implementamos el método `toString()` como utilizad para depuración en el desarrollo de los ejercicios que utilicen la ontología. Cuando ya tengamos nuestra aplicación bien desarrollada no necesitaremos utilizar este método de los elementos de la ontología. 

Un elemento importante que debemos fijarnos son en las anotaciones que podemos incluir en la definición. Las anotaciones que tenemos a nuestra disposición las podemos encontrar en [jade.content.onto.annotations](https://jade.tilab.com/doc/api/jade/content/onto/annotations/package-frame.html). Yo me centraré en dos de ellas, por ser las que utilizaremos en las ontologías de la asignatura y en el tutorial[^nota1] podemos encontrar más información con otras anotaciones.

- `@Slot(mandatory=true)` : lo vamos a utilizar para indicar que el atributo será obligatorio, esto es, provocará una excepción que deberemos tratar adecuadamente si alguno de los elementos de la ontología no esté correctamente construido cuando intentamos generar el contenido del mensaje **ACL** o recuperamos el contenido del mensaje **ACL**.
- `@AggregateSlot(cardMin=1, type=Operando.class)` : cuando utilicemos colecciones, atributos que estarán conformados por más de un valor, podemos indicar el número de elementos que pueden conformarlo y el tipo del elemento de la ontología. Conviene que utilicemos las colecciones de la forma más restrictiva posible, esto es, no debemos utilizarlas para incluir cualquier cosa. Eso nos ayudará a no tener que hacer un procesamiento innecesario para este tipo de información. Si no se cumple con las restricciones se generan excepciones como se ha explicado en el punto anterior.

## 4.5 Modificaciones a los agentes y tareas de la Sesión 3

Por último queda comentar las modificaciones necesarias al proyecto que se presentó en en guión de la [Sesión 3](https://gitlab.com/ssmmaa/guionsesion3).

### 4.5.1 Interface Constantes

Como parte de los elementos que formaban parte de esta interface ahora serán elementos del vocabulario de la ontología solo incluiremos aquellos elementos necesarios para la ejecución de las tareas de los agentes. No se debe incluir ningún elemento necesario para la comunicación entre los agentes. Todos los elementos para la comunicación deberán ser incluidos en el diseño de la ontología.

```java
public interface Constantes {
    public static final long ESPERA = 4000; // 4 segundos
    public static final long TIME_OUT = 2000; // 2 segundos;
    public static final int NO_ENCONTRADO = -1;
    public static final int PRIMERO = 0;
    public static final Random aleatorio = new Random();
    public static final int D100 = 100; // Representa un dado de 100
    public static final int ACEPTAR = 85; // 85% de aceptación para la operación 
}
```

### 4.5.2 Interface Consola

Esta es la inteface principal que permite el uso de las tareas por parte de los agentes. Ahora hay que incluir métodos necesarios para la utilización de las utilidades de la biblioteca Jade para trabajar con las ontologías:

```java
...
public NombreServicio getServicio();
    
// Métodos de acceso para trabajar con la ontología del agente
public ContentManager getManager();
public Codec getCodec();
public Ontology getOntology(); 
...
```

Para poder crear mensajes **ACL** o extraer el contenido de un mensaje **ACL** necesitamos poder acceder a la configuración del `ContentManager`, `Codec` y `Ontology` del agente que utiliza la tarea. Además creamos un método de acceso para obtener el `NombreServicio` del agente por la forma en que se ha diseñado la ontología.

El resto de interfaces no sufren cambios.

### 4.5.3 Tareas que implementan los protocolos Jade

Se presentan los diagramas UML que muestran el intercambio de los elementos de ontología entre los agentes que participan en el protocolo.

#### TareaRealizarOperacion
```mermaid
sequenceDiagram
	participant i as AgenteFormulario 
	participant r as AgenteOperacion
	i->>r: REQUEST (SolicitarOperacion) 
	Note right of r: Petición de una<br/>operación
	alt desconocido
		r-->>i: NOT_UNDERSTOOD (Respuesta)
    else rechazo
	    r-->>i: REFUSE (Respuesta)
	else conforme
		r-->>i: AGREE (Respuesta)
	end
	Note right of r: Resolución de la<br/>operación
	alt no definida
		r->>i: FAILURE (Respuesta)
	else completada
		r->>i: INFORM (Resultado)
	end
```

#### Crear el mensaje FIPA-Request

El método `enviarPunto2D()` era el encargado para añadir la tarea para el **rol iniciador**. Ahora que se utiliza una ontología debemos añadir nuevos campos al `ACLMessage` que antes no se incluían:

```java
...
msg.setLanguage(codec.getName());
msg.setOntology(ontology.getName());
...
```

Se debe establecer el *lenguaje* y la *ontología* para que el `ContentManager` funcione correctamente. No incluir estas modificaciones generará errores a la hora de completar el contenido del mensaje.

Antes de incluir el contenido del mensaje se tiene que completar el `ContentElement` que formará el contenido del mensaje **FIPA-Request**. Como será un `AgentAction` no podremos incluirlo directamente en el mensaje y necesitaremos añadirlo a un objeto `Action`:

```java
...
// Generamos una operación aleatoria
operacion = OPERACIONES[aleatorio.nextInt(OPERACIONES.length)];
// Añadimos el punto a la lista de operandos
operandos.add((Punto2D)punto);
        
// Generamos el contenido del mensaje
op = new RealizarOperacion(operacion,operandos);
// Debemos crear la acción para poder completar el contenido del mensaje
ac = new Action(this.getAID(),op);
...
```

En este caso además utilizamos una colección que deberá ser un elemento de los presentes en el paquete [jade.util.leap](https://jade.tilab.com/doc/api/jade/util/leap/package-frame.html):

```java
...
List operandos = new jade.util.leap.ArrayList();
...
```

Es importante tener esto presente porque si no se utilizan las clases definidas en `jade.util.leap` se generará una excepción al completar el contenido del mensaje.

Ya solo necesitamos incluir el contenido del mensaje mediante el método `fillContent(.)`:

```java
...
try {
    // Completamos el contenido del mensaje
    manager.fillContent(msg, ac);
            
    addBehaviour(new TareaRealizarOperacionIniciador(this,msg));
} catch (Codec.CodecException | OntologyException ex) {
    Logger.getLogger(AgenteFormulario.class.getName()).log(Level.SEVERE, null, ex);
}
...
```

Podemos observar que hay que tratar posibles excepciones que se pueden producir. Se deja una gestión por defecto porque solo se producirá un error si no hemos generado correctamente el contenido del mensaje y eso siempre será culpa del programador no que el agente no esté funcionando correctamente. Nos ayudará a depurar el código para garantizar que el agente crea el mensaje **ACL** correctamente mediante la definición de la ontología.

#### TareaRealizarOperacionIniciador

En esta tarea lo único que se modifica es la forma que recupera la información del mensaje `ACL` que se recibe. Se modifica el constructor para poder obtener el `ContentManager` del agente: `this.manager = this.agente.getManager();`.

Para recuperar el contenido del mensaje utilizamos el método `extractContent(.)`:

```java
...
while (it.hasNext()) {
    MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
    ACLMessage msg = (ACLMessage) it.next();
    opAgent = msg.getSender();
            
    try { 
        msgInfo = (Respuesta) manager.extractContent(msg);
...
```

Ahora las excepciones que se tienen que tratar son:

```java
...} catch ( Codec.CodecException | OntologyException ex ) {
       msgConsola.setContenido(opAgent.getLocalName() +
                               " El contenido del mensaje es incorrecto\n\t"
                               + ex);
   }
...
```

Si se produce alguna excepción a la hora de obtener la información del contenido del mensaje nos indica que el agente que está enviando la información no la ha generado correctamente y la tarea no podrá realizar la operación para la que ha sido diseñada. Este es un ejemplo sencillo pero deberemos adaptar la decisión dependiendo del problema que se esté resolviendo.

#### TareaRealizarOperacionParticipante

Como en el caso anterior se modificará el constructor para tener los elementos necesarios para el uso de la ontología:

```java
...
this.manager = this.agente.getManager();
this.ontology = this.agente.getOntology();
...
```

El mayor cambio que se realiza en esta tarea, y en las que conforman el **rol participante**, es que dejamos de utilizar las excepciones para responder a la comunicación con diferentes *intenciones* según el protocolo implicado. Se podrían seguir utilizando las excepciones pero deberíamos utilizar el constructor en el que se le pasa el `ACLMessage` completo que se quiere enviar. Pero como quiero centralizar en un único punto las excepciones que se pueden producir al generar el mensaje he optado por otra opción:

```java
...
MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
Respuesta msgInfo = new Respuesta(this.agente.getServicio());
        
ACLMessage msg = request.createReply();
        
if ( !request.getOntology().equals(ontology.getName()) ) {
    msg.setPerformative(ACLMessage.NOT_UNDERSTOOD);
    msgInfo.setJustificacion(Incidencia.ONTOLOGIA_DESCONOCIDA);
    msgConsola.setContenido(msgInfo.toString());
} else if ( !aceptarOperacion() ) {
    msg.setPerformative(ACLMessage.REFUSE);
    msgInfo.setJustificacion(Incidencia.OPERACIONES_SUPERADAS);
    msgConsola.setContenido(msgInfo.toString());
} else {
    msg.setPerformative(ACLMessage.AGREE);
    msgInfo.setJustificacion(Incidencia.OPERACION_ACEPTADA);
    msgConsola.setContenido(msgInfo.toString());
}
        
try {
    manager.fillContent(msg, msgInfo);
} catch (Codec.CodecException | OntologyException ex) {
    Logger.getLogger(TareaRealizarOperacionParticipante.class.getName()).log(Level.SEVERE, null, ex);
}
            
agente.setMensaje(msgConsola);
return msg;
...
```

Se completan los diferentes elementos que conformarán el mensaje atendiendo a las posibles opciones que se nos pueden presentar. Así se centraliza en un único punto completar el contenido del mensaje. Como en otras ocasiones, si se produce una excepción al generar el contenido del mensaje es culpa del programador y no del mal funcionamiento del agente, por eso dejo el tratamiento por defecto de las excepciones.

Para el método `prepareResultNotification(.)` es similar, pero primero debemos obtener el contenido del mensaje de la propuesta:

```java
...
MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
ACLMessage msg = request.createReply();
Respuesta respuesta = new Respuesta(agente.getServicio());
Resultado resultado = null;
msg.setPerformative(ACLMessage.INFORM);
        
try {
    Action ac = (Action) manager.extractContent(request);
    RealizarOperacion op = (RealizarOperacion) ac.getAction();
    Punto2D punto2D = (Punto2D) op.getOperandos().get(PRIMERO);
        
    resultado = operacion(op.getOperacion(), punto2D);
    msgConsola.setContenido(resultado.toString());
...
```

Podemos observar que el contenido del mensaje será un objeto `Action` donde se incluye el elemento de ontología deseado. Tenemos que tener esto presenta cuando recuperemos la información del mensaje. Se pueden producir diferentes excepciones, o no se puede completar la operación o el contenido del mensaje no es correcto. Hay que diferenciar los dos casos.

#### TareaSubConsola

```mermaid
sequenceDiagram
	participant i as AgenteConsola 
	participant r1 as AgenteFormulario
	participant r2 as AgenteOperacion
	i->>r1: SUBSCRIBE(SolicitarSubscripcion)
	Note right of r1: Solicitud de subscrip<br/>ción
	i->>r2: SUBSCRIBE(SolicitarSubscripcion) 
	Note right of r2: Solicitud de subscrip<br/>ción
	alt rechazo
		r1-->>i: REFUSE (Respuesta)
	else problemas
		r1-->>i: FAILURE (Respuesta)
	else acepta
		r1-->>i: AGREE (Respuesta)
	end
	alt rechazo
		r2-->>i: REFUSE (Respuesta)
	else problemas
		r2-->>i: FAILURE (Respuesta)
	else acepta
		r2-->>i: AGREE (Respuesta)
	end
	r1-->>i: INFORM (MensajeConsola)
	Note right of r1: Informes de subscrip<br/>ción
	r2-->>i: INFORM (MensajeConsola)
	Note right of r2: Informes de subscrip<br/>ción
```

Las modificaciones para esta tarea serán similares a las presentadas en la tarea anterior. Lo importante es comprobar los elementos de ontología que están presentes en el intercambio de los mensajes del protocolo. Como en el caso anterior el envío del mensaje del **agente iniciador** es un objeto `Action()`. El resto de mensajes están compuestos por elementos `Predicate` que pueden incluirse directamente en el contenido del mensaje. Revisar el código de las tareas.

## 4.5 Modificación propuesta

Se propone una modificación opcional partiendo del [proyecto](https://gitlab.com/ssmmaa/guionsesion4/-/archive/v1.1/guionsesion4-v1.1.zip) del ejemplo realizar las modificaciones necesarias para incluir el `AgenteMonitor` propuesto en el guión de la [Sesión 2](https://gitlab.com/ssmmaa/guionsesion2#27-pr%C3%A1ctica-a-realizar). Para ello deberán realizarse los añadidos necesarios a la ontología y el rediseño de las tareas representadas por los protocolos utilizados por el `AgenteMonitor`.


---
[^nota1]: Este [tutorial](https://jade.tilab.com/doc/tutorials/BeanOntologyTutorial.pdf) demuestra las diferentes particularidades del uso de ontologías en Jade.

[imagen1]:https://gitlab.com/ssmmaa/guionsesion4/-/raw/master/img/imagen1.png
[imagen2]:https://gitlab.com/ssmmaa/guionsesion4/-/raw/master/img/imagen2.png
[imagen3]:https://gitlab.com/ssmmaa/guionsesion4/-/raw/master/img/imagen3.png
[imagen4]:https://gitlab.com/ssmmaa/guionsesion4/-/raw/master/img/imagen4.png
[imagen5]:https://gitlab.com/ssmmaa/guionsesion4/-/raw/master/img/imagen5.png
<!--stackedit_data:
eyJoaXN0b3J5IjpbOTk0MjQwNzkzLC0zOTM0MjkwMTgsLTEwOT
kzNDc1NDRdfQ==
-->