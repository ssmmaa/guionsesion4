/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    public static final long ESPERA = 4000; // 4 segundos
    public static final long TIME_OUT = 2000; // 2 segundos;
    public static final int NO_ENCONTRADO = -1;
    public static final int PRIMERO = 0;
    public static final Random aleatorio = new Random();
    public static final int D100 = 100; // Representa un dado de 100
    public static final int ACEPTAR = 85; // 85% de aceptación para la operación 
}
