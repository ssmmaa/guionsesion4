/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.agentes;

import static es.uja.ssmmaa.guionsesion4.Constantes.ESPERA;
import static es.uja.ssmmaa.guionsesion4.Constantes.NO_ENCONTRADO;
import static es.uja.ssmmaa.guionsesion4.Constantes.TIME_OUT;
import static es.uja.ssmmaa.guionsesion4.Constantes.aleatorio;
import es.uja.ssmmaa.guionsesion4.gui.FormularioJFrame;
import es.uja.ssmmaa.guionsesion4.ontologia.OntoSesion4;
import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.NombreServicio;
import static es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.NombreServicio.FORMULARIO;
import static es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.OPERACIONES;
import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.Operacion;
import static es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.TipoServicio.GUI;
import static es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.TipoServicio.UTILIDAD;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.Punto2D;
import es.uja.ssmmaa.guionsesion4.tareas.EnvioConsola;
import es.uja.ssmmaa.guionsesion4.tareas.SubscripcionDF;
import es.uja.ssmmaa.guionsesion4.tareas.TareaEnvioMensajes;
import es.uja.ssmmaa.guionsesion4.tareas.TareaSubConsolaParticipante;
import es.uja.ssmmaa.guionsesion4.tareas.TareaSubscripcionDF;
import es.uja.ssmmaa.guionsesion4.util.GestorSubscripciones;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.MensajeConsola;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.RealizarOperacion;
import es.uja.ssmmaa.guionsesion4.tareas.TareaRealizarOperacionIniciador;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import java.util.ArrayList;
import jade.domain.FIPANames;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pedroj
 * Esqueleto de agente para la estructura general que deben tener todos los
 * agentes
 */
public class AgenteFormulario extends Agent implements SubscripcionDF, 
                                                EnvioConsola<MensajeConsola> {
    // Variables del agente
    private FormularioJFrame myGui;
    private ArrayList<AID> listaAgentes;
    private GestorSubscripciones gestor;
    private ArrayList<MensajeConsola> mensajesPendientes;
    
    // Para trabajar con la ontología
    private final ContentManager manager = getContentManager();
	
    // El lenguaje utilizado por el agente para la comunicación es SL 
    private final Codec codec = new SLCodec();

    // La ontología que utilizará el agente
    private Ontology ontology;

    @Override
    protected void setup() {
       //Inicialización de las variables del agente
       mensajesPendientes = new ArrayList();
       listaAgentes = new ArrayList();
       gestor = new GestorSubscripciones();
       
       //Configuración del GUI
       myGui = new FormularioJFrame(this);
       myGui.setVisible(true);
       
       //Registro del agente en las Páginas Amarrillas
       DFAgentDescription dfd = new DFAgentDescription();
       dfd.setName(getAID());
       ServiceDescription sd = new ServiceDescription();
       sd.setType(GUI.name());
       sd.setName(FORMULARIO.name());
       dfd.addServices(sd);
       try {
           DFService.register(this, dfd);
       } catch (FIPAException fe) {
            fe.printStackTrace();
       }
       
       //Registro de la Ontología
       try {
            ontology = OntoSesion4.getInstance();
        } catch (BeanOntologyException ex) {
            Logger.getLogger(AgenteFormulario.class.getName()).log(Level.SEVERE, null, ex);
        }
        manager.registerLanguage(codec);
	manager.registerOntology(ontology);
       
       System.out.println("Se inicia la ejecución del agente: " + this.getName());
       //Añadir las tareas principales
       addBehaviour(new TareaEnvioMensajes(this,ESPERA));
       
       // Plantilla del mensaje de suscripción
       MessageTemplate plantilla;
       plantilla= MessageTemplate.and(
                    MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()), 
                                           MessageTemplate.MatchSender(this.getAMS()))), 
                    MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE));
       addBehaviour(new TareaSubConsolaParticipante(this,plantilla,gestor));
       
       //Suscripción al servicio de páginas amarillas
       //Para localiar a los agentes operación y consola
       DFAgentDescription template = new DFAgentDescription();
       ServiceDescription templateSd = new ServiceDescription();
       templateSd.setType(UTILIDAD.name());
       template.addServices(templateSd);
       addBehaviour(new TareaSubscripcionDF(this,template));
    }

    @Override
    protected void takeDown() {
       //Eliminar registro del agente en las Páginas Amarillas
       try {
            DFService.deregister(this);
	}
            catch (FIPAException fe) {
            fe.printStackTrace();
	}
       
       //Liberación de recursos, incluido el GUI
       myGui.dispose();
       
       //Despedida
       System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }

    // Métodos de acceso para trabajar con la ontología del agente
    public ContentManager getManager() {
        return manager;
    }

    public Codec getCodec() {
        return codec;
    }

    public Ontology getOntology() {
        return ontology;
    }
    
    //Métodos de trabajo del agente
    @Override
    public void addAgent(AID agente, NombreServicio servicio) {
        if( listaAgentes.indexOf(agente) == NO_ENCONTRADO )
            listaAgentes.add(agente);
        
        if ( !listaAgentes.isEmpty() )
            myGui.activarEnviar(true);
    }

    @Override
    public boolean removeAgent(AID agente, NombreServicio servicio) {
        boolean resultado = listaAgentes.remove(agente);
        
        if ( listaAgentes.isEmpty() )
            myGui.activarEnviar(false);
        
        return resultado;
    }

    public void enviarPunto2D(Punto2D punto) {
        Action ac;
        RealizarOperacion op;
        Operacion operacion;
        // Lista de Jade para crear el contenido
        List operandos = new jade.util.leap.ArrayList();  
        
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology.getName());
        for( AID agenteOp : listaAgentes )
            msg.addReceiver(agenteOp);
        
        // Generamos una operación aleatoria
        operacion = OPERACIONES[aleatorio.nextInt(OPERACIONES.length)];
        // Añadimos el punto a la lista de operandos
        operandos.add((Punto2D)punto);
        
        // Generamos el contenido del mensaje
        op = new RealizarOperacion(operacion,operandos);
        // Debemos crear la acción para poder completar el contenido del mensaje
        ac = new Action(this.getAID(),op);
        
        try {
            // Completamos el contenido del mensaje
            manager.fillContent(msg, ac);
            
            addBehaviour(new TareaRealizarOperacionIniciador(this,msg));
        } catch (Codec.CodecException | OntologyException ex) {
            Logger.getLogger(AgenteFormulario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public GestorSubscripciones getGestor() {
        return gestor;
    }

    @Override
    public void setMensaje(MensajeConsola msg) {
        mensajesPendientes.add(msg);
    }

    @Override
    public ArrayList<MensajeConsola> getMensajes() {
        return mensajesPendientes;
    }

    @Override
    public NombreServicio getServicio() {
        return FORMULARIO;
    }
}
