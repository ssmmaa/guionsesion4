/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.ontologia;

/**
 *
 * @author pedroj
 */
public interface Vocabulario {
    public enum TipoServicio {
        GUI, UTILIDAD, SISTEMA; 
    }
    public static final TipoServicio[] TIPOS = TipoServicio.values();
    public enum NombreServicio {
        OPERACION, CONSOLA, FORMULARIO;
    }
    public static final NombreServicio[] SERVICIOS = NombreServicio.values();
    
    public enum Operacion {
        SUMA, RESTA, MULTIPLICACION, DIVISION;
    }
    public static final Operacion[] OPERACIONES = Operacion.values();
    
    public static enum Incidencia {
        CANCELACION, ERROR_CANCELACION, ERROR_AGENTE, ERROR_MENSAJE_INCORRECTO, ERROR_CONTENIDO_MENSAJE,
        ONTOLOGIA_DESCONOCIDA, OPERACION_NO_DEFINIDA, OPERACIONES_SUPERADAS, OPERACION_ACEPTADA,
        SUBSCRIPCION_CANCELADA, SUBSCRIPCION_CREADA, ERROR_REGISTRO_SUBSCRIPCION
    } 
}
