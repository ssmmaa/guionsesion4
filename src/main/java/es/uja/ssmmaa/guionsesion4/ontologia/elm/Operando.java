/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.ontologia.elm;

import jade.content.Concept;

/**
 * Concepto abstracto para facilitar la extensión de la ontología y poder
 * añadir operndos a diferentes operaciones que se definan más adelante
 * @author pedroj
 */
public abstract class Operando implements Concept {
    
}
