/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.ontologia.elm;

import jade.content.onto.annotations.Slot;

/**
 *
 * @author pedroj
 */
public class Punto2D extends Operando {
    private double x;
    private double y;

    public Punto2D() {
        this.x = 0;
        this.y = 0;
    }

    
    public Punto2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Slot(mandatory=true)
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    @Slot(mandatory=true)
    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Punto 2D ( " + x + " , " + y + " )";
    }
}
