/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.ontologia.elm;

import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.Operacion;
import jade.content.AgentAction;
import jade.content.onto.annotations.AggregateSlot;
import jade.content.onto.annotations.Slot;
import jade.util.leap.List;

/**
 *
 * @author pedroj
 */
public class RealizarOperacion implements AgentAction {
    private Operacion operacion;
    private List operandos;

    public RealizarOperacion() {
        this.operacion = null;
        this.operandos = null;
    }

    public RealizarOperacion(Operacion operacion, List operandos) {
        this.operacion = operacion;
        this.operandos = operandos;
    }

    @Slot(mandatory=true)
    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    @AggregateSlot(cardMin=1, type=Operando.class)
    public List getOperandos() {
        return operandos;
    }

    public void setOperandos(List operandos) {
        this.operandos = operandos;
    }

    @Override
    public String toString() {
        return "RealizarOperacion{" + "operacion=" + operacion + ", operandos=" + operandos + '}';
    }
}
