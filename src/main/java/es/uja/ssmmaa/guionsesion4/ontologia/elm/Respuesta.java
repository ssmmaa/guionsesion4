/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.ontologia.elm;

import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.Incidencia;
import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.NombreServicio;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

/**
 *
 * @author pedroj
 */
public class Respuesta implements Predicate {
    private NombreServicio servicio;
    private Incidencia justificacion;

    public Respuesta() {
        this.servicio = null;
        this.justificacion = null;
    }

    public Respuesta(NombreServicio servicio) {
        this.servicio = servicio;
        this.justificacion = null;
    }
    
    public Respuesta(NombreServicio servicio, Incidencia justificacion) {
        this.servicio = servicio;
        this.justificacion = justificacion;
    }

    @Slot(mandatory=true)
    public NombreServicio getServicio() {
        return servicio;
    }

    public void setServicio(NombreServicio servicio) {
        this.servicio = servicio;
    }

    @Slot(mandatory=true)
    public Incidencia getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(Incidencia justificacion) {
        this.justificacion = justificacion;
    }

    @Override
    public String toString() {
        return "Respuesta{" + "servicio=" + servicio + ", justificacion=" + justificacion + '}';
    }
}
