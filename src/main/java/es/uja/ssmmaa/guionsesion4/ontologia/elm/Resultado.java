/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.ontologia.elm;

import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.NombreServicio;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

/**
 *
 * @author pedroj
 */
public class Resultado implements Predicate {
    private NombreServicio servicio;
    private String contenido;

    public Resultado() {
        this.servicio = null;
        this.contenido = null;
    }

    public Resultado(NombreServicio servicio) {
        this.servicio = servicio;
        this.contenido = null;
    }
    
    public Resultado(NombreServicio servicio, String contenido) {
        this.servicio = servicio;
        this.contenido = contenido;
    }

    @Slot(mandatory=true)
    public NombreServicio getServicio() {
        return servicio;
    }

    public void setServicio(NombreServicio servicio) {
        this.servicio = servicio;
    }

    @Slot(mandatory=true)
    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    @Override
    public String toString() {
        return "Resultado{" + "servicio=" + servicio + ", contenido=" + contenido + '}';
    }
}
