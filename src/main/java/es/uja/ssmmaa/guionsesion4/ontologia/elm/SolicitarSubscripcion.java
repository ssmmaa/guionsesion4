/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.ontologia.elm;

import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.NombreServicio;
import jade.content.AgentAction;
import jade.content.onto.annotations.Slot;
import jade.core.AID;

/**
 *
 * @author pedroj
 */
public class SolicitarSubscripcion implements AgentAction {
    private AID agente;
    private NombreServicio servicio;

    public SolicitarSubscripcion() {
        this.agente = null;
        this.servicio = null;
    }

    public SolicitarSubscripcion(AID agente, NombreServicio servicio) {
        this.agente = agente;
        this.servicio = servicio;
    }

    @Slot(mandatory=true)
    public AID getAgente() {
        return agente;
    }

    public void setAgente(AID agente) {
        this.agente = agente;
    }

    @Slot(mandatory=true)
    public NombreServicio getServicio() {
        return servicio;
    }

    public void setServicio(NombreServicio servicio) {
        this.servicio = servicio;
    }

    @Override
    public String toString() {
        return "SolicitarSusbcripcion{" + "agente=" + agente + ", servicio=" + servicio + '}';
    }
}
