/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.tareas;

import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.NombreServicio;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.Ontology;

/**
 * Interface definida para las tareas que estén diseñadas en generar mensajes
 * que se presenten posteriormente en una ventana que cumpla con las cracterísticas
 * de una consola para un agente.
 * @author pedroj
 */
public interface Consola<T> {
    public void setMensaje(T msg);
    public NombreServicio getServicio();
    
    // Métodos de acceso para trabajar con la ontología del agente
    public ContentManager getManager();
    public Codec getCodec();
    public Ontology getOntology(); 
}
