/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.tareas;

import es.uja.ssmmaa.guionsesion4.util.GestorSubscripciones;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.MensajeConsola;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionResponder.Subscription;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tarea que se repetirá cada intervalo de tiempo ESPERA para comprobar si hay
 * mensajes que enviar a agentes que previamente han formalizado una subscripción.
 * El tipo de mensaje que se envía es MensajeConsola.
 * @author pedroj
 */
public class TareaEnvioMensajes extends TickerBehaviour {
    private final EnvioConsola<MensajeConsola> agente;

    public TareaEnvioMensajes(Agent a, long period) {
        super(a, period);
        
        this.agente = (EnvioConsola) a;
    }

    /**
     * Tarea que explora cíclicamente si tenemos mensajes pendientes que enviar
     * a todas las subscripciones con AgenteConsola activas.
     */
    @Override
    protected void onTick() {
        GestorSubscripciones gestor = agente.getGestor();
        List<MensajeConsola> mensajes = agente.getMensajes();
        
        // Hay mensajes pendientes y subscripciones activas
        if( !gestor.isEmpty() && !mensajes.isEmpty() )
            // Para todas las subscripciones activas
            for( Subscription subscripcion : gestor.values() ) 
                enviar(subscripcion, mensajes);
        
        mensajes.clear();
    }
    
    /**
     * Envia los mensajes pendientes a las subscripciones de las consolas
     * conocidas
     * @param subscripcion
     *          Subscripción a un AgenteConsola acriva
     * @param mensajes 
     *          Lista de MensajeConsola a enviar
     */
    private void enviar( Subscription subscripcion, List<MensajeConsola> mensajes ) {
        ContentManager manager = agente.getManager();
        Codec codec = agente.getCodec();
        Ontology ontology = agente.getOntology();
        
        for( MensajeConsola contenido : mensajes ) {
            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
            msg.setLanguage(codec.getName());
            msg.setOntology(ontology.getName());
            
            try {
                manager.fillContent(msg, contenido);
                
                // Enviamos el mensaje al suscriptor
                subscripcion.notify(msg);
            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(TareaEnvioMensajes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
