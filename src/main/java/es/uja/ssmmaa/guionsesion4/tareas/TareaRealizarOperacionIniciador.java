/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.tareas;

import es.uja.ssmmaa.guionsesion4.ontologia.elm.MensajeConsola;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.Respuesta;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.Resultado;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import java.util.Iterator;
import java.util.Vector;

/**
 * Protocolo Request para el rol iniciador que solicitará la realización de una
 * operación definida en el enumerado Operacion para un objeto Punto2D.
 * Se generan mensajes del tipo MensajeConsola con el resultado que se genera en
 * la tarea.
 * @author pedroj
 */
public class TareaRealizarOperacionIniciador extends AchieveREInitiator {
    private final Consola<MensajeConsola> agente;
    private AID opAgent;
    
    // Para que la tarea pueda trabajar con la ontología del agente
    private final ContentManager manager;
    
    public TareaRealizarOperacionIniciador(Agent a, ACLMessage msg) {
        super(a, msg);
        
        this.agente = (Consola) a;
        this.opAgent = null;
        
        this.manager = this.agente.getManager();
    }

    /**
     * Posibles respuestas a la solicitud de realización de la operación a los
     * AgenteOperacion. Los agentes indican su intención a la solicitud de
     * la operación.
     * @param responses
     *          Mensajes ACL con las respuestas de los agentes participantes
     */
    @Override
    protected void handleAllResponses(Vector responses) {
        Respuesta msgInfo;
        Iterator it = responses.iterator();
        
        while (it.hasNext()) {
            MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
            ACLMessage msg = (ACLMessage) it.next();
            opAgent = msg.getSender();
            
            try { 
                msgInfo = (Respuesta) manager.extractContent(msg);
            
                switch ( msg.getPerformative() ) {
                    case ACLMessage.NOT_UNDERSTOOD:
                        msgConsola.setContenido(opAgent.getLocalName() + " NO HA ENTENDIDO " 
                                + msgInfo.getJustificacion());
                        break;
                    case ACLMessage.REFUSE:
                        msgConsola.setContenido(opAgent.getLocalName() + " Se ha RECHAZADO  " 
                                + msgInfo.getJustificacion());
                        break;
                    case ACLMessage.AGREE:
                        msgConsola.setContenido(opAgent.getLocalName() + " " + msgInfo.getJustificacion());
                        break;
                    default:
                        msgConsola.setContenido(opAgent.getLocalName() +
                                           " envia un mensaje desconocido\n\t" +
                                            msg.getContent());
                        break;
                }
            } catch ( Codec.CodecException | OntologyException ex ) {
                msgConsola.setContenido(opAgent.getLocalName() +
                                          " El contenido del mensaje es incorrecto\n\t"
                                          + ex);
            }
            
            agente.setMensaje(msgConsola);
        }
    }

    /**
     * Resultado para la operación solicitada de los AgenteOperación que aceptaron
     * la realización.
     * @param resultNotifications 
     *          Mensaje ACL con el resultado de la operación.
     */
    @Override
    protected void handleAllResultNotifications(Vector resultNotifications) {
        Iterator it = resultNotifications.iterator();
        
        while (it.hasNext()) {
            MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
            ACLMessage msg = (ACLMessage) it.next();
            opAgent = msg.getSender();
            
            try {
                switch ( msg.getPerformative() ) {
                    case ACLMessage.FAILURE:
                        Respuesta respuesta = (Respuesta) manager.extractContent(msg);
                        msgConsola.setContenido(opAgent.getLocalName() + " Operación FALLIDA " 
                                                + respuesta.getJustificacion());
                        break;

                    case ACLMessage.INFORM:
                        Resultado resultado = (Resultado) manager.extractContent(msg);
                        msgConsola.setContenido(opAgent.getLocalName() + " " + resultado.getContenido());
                        break;

                    default:
                        msgConsola.setContenido(opAgent.getLocalName() +
                                           " envia un mensaje desconocido\n\t" +
                                            msg.getContent());
                        break;
                }
            } catch ( Codec.CodecException | OntologyException ex ) {
                msgConsola.setContenido(opAgent.getLocalName() +
                                          " El contenido del mensaje es incorrecto\n\t"
                                          + ex);
            }
            
            agente.setMensaje(msgConsola);
        }
    }
    
    /**
     * Si algún mensaje llega fuera de la secuencia esperada para el protocolo
     * Request.
     * @param msg
     *          Mensaje ACL que ha llegado fuera de la secuencia.
     */
    @Override
    protected void handleOutOfSequence(ACLMessage msg) {
        MensajeConsola msgConsola;
        
        msgConsola = new MensajeConsola(msg.getSender().getName(),
                                       msg.getContent());
        
        agente.setMensaje(msgConsola);
    }
}
