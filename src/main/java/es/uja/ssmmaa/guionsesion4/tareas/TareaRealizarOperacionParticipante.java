/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.tareas;

import static es.uja.ssmmaa.guionsesion4.Constantes.ACEPTAR;
import static es.uja.ssmmaa.guionsesion4.Constantes.D100;
import static es.uja.ssmmaa.guionsesion4.Constantes.PRIMERO;
import static es.uja.ssmmaa.guionsesion4.Constantes.aleatorio;
import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.Incidencia;
import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.Operacion;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.MensajeConsola;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.Punto2D;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.RealizarOperacion;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.Respuesta;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.Resultado;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Protocolo Request para el rol de participante. Los AgenteOperacion deciden
 * la realización de la operación sobre un Punto2D y comunican el resultado.
 * @author pedroj
 */
public class TareaRealizarOperacionParticipante extends AchieveREResponder {
    private final Consola<MensajeConsola> agente;
    
    // Para que la tarea pueda trabajar con la ontología del agente
    private final ContentManager manager;
    private final Ontology ontology;

    public TareaRealizarOperacionParticipante(Agent a, MessageTemplate mt) {
        super(a, mt);
        
        this.agente = (Consola) a;
        
        this.manager = this.agente.getManager();
        this.ontology = this.agente.getOntology();
    }

    /**
     * Se toma la decisión para realizar la operación solicitada. Utilizamos un
     * % ACEPTAR para decidir si se acepta o no la realización. Simula que el
     * agente puede tomar la decisión para realizar o no la acción.
     * @param request
     *          Mensaje ACL con el Punto2D.
     * @return
     *          Mensaje ACL con el resultado aceptando.
     * @throws RefuseException
     *          Mensaje ACL con el resultado del rechazo.
     * @throws NotUnderstoodException 
     *          Mensaje ACL si no hay contenido en el mensaje.
     */
    @Override
    protected ACLMessage handleRequest(ACLMessage request) throws RefuseException, NotUnderstoodException {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        Respuesta msgInfo = new Respuesta(this.agente.getServicio());
        
        ACLMessage msg = request.createReply();
        
        if ( !request.getOntology().equals(ontology.getName()) ) {
            msg.setPerformative(ACLMessage.NOT_UNDERSTOOD);
            msgInfo.setJustificacion(Incidencia.ONTOLOGIA_DESCONOCIDA);
            msgConsola.setContenido(msgInfo.toString());
        } else if ( !aceptarOperacion() ) {
            msg.setPerformative(ACLMessage.REFUSE);
            msgInfo.setJustificacion(Incidencia.OPERACIONES_SUPERADAS);
            msgConsola.setContenido(msgInfo.toString());
        } else {
            msg.setPerformative(ACLMessage.AGREE);
            msgInfo.setJustificacion(Incidencia.OPERACION_ACEPTADA);
            msgConsola.setContenido(msgInfo.toString());
        }
        
        try {
            manager.fillContent(msg, msgInfo);
        } catch (Codec.CodecException | OntologyException ex) {
            Logger.getLogger(TareaRealizarOperacionParticipante.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        agente.setMensaje(msgConsola);
        return msg;
    }

    /**
     * Se realiza una operación aleatoria de entre Operacion sobre el Punto2D 
     * y se envía el resultado de la operación como resultado.
     * @param request
     *          Mensaje ACL con el Punto2D.
     * @param response
     *          Mensaje ACL con la respuesta enviada en el método anterior.
     * @return
     *          Mensaje ACL con el resultado de la operación.
     * @throws FailureException 
     *          Mensaje ACL con el motivo del fallo de la operación solicitada.
     */
    @Override
    protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response) 
                                                                    throws FailureException {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        ACLMessage msg = request.createReply();
        Respuesta respuesta = new Respuesta(agente.getServicio());
        Resultado resultado = null;
        msg.setPerformative(ACLMessage.INFORM);
        
        try {
            Action ac = (Action) manager.extractContent(request);
            RealizarOperacion op = (RealizarOperacion) ac.getAction();
            Punto2D punto2D = (Punto2D) op.getOperandos().get(PRIMERO);
        
            resultado = operacion(op.getOperacion(), punto2D);
            msgConsola.setContenido(resultado.toString());
            
        } catch ( Codec.CodecException | OntologyException ex ) {
            msg.setPerformative(ACLMessage.FAILURE);
            respuesta.setJustificacion(Incidencia.ERROR_CONTENIDO_MENSAJE);
            msgConsola.setContenido(respuesta.toString());
        } catch ( ArithmeticException ex ) {
            msg.setPerformative(ACLMessage.FAILURE);
            respuesta.setJustificacion(Incidencia.OPERACION_NO_DEFINIDA);
            msgConsola.setContenido(respuesta.toString());
        } 
        
        try {
            // Completamos el contenido del mensaje con el elemento de ontología
            // apropiado dependiendo del resultado de la operación solicitada
            if( msg.getPerformative() == ACLMessage.FAILURE )
                manager.fillContent(msg, respuesta);
            else 
                manager.fillContent(msg, resultado);
        } catch (Codec.CodecException | OntologyException ex) {
            Logger.getLogger(TareaRealizarOperacionParticipante.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        agente.setMensaje(msgConsola);
        return msg;
    }
    
    /**
     * Simula la toma de la decisión para la realización o no de la operación.
     * @return 
     *      true si se realiza la operación y false si se rechaza.
     */
    private boolean aceptarOperacion() {
        return aleatorio.nextInt(D100) < ACEPTAR;
    }
    
    /**
     * Se realiza una operación aleatoria de entre las disponibles en Operaciones 
     * @param punto
     *          Punto2D para realizar la operación.
     * @throws ArithmeticException 
     *          Si la operación no está definida para los elementos del Punto2D.
     */
    private Resultado operacion (Operacion op, Punto2D punto) throws ArithmeticException {
        Resultado calculo = new Resultado(agente.getServicio());
        double resultado;
        
        switch ( op.ordinal() ) {
            case 0:
                //Suma
                resultado = punto.getX() + punto.getY();
                calculo.setContenido("Se ha realizado la suma de " + punto
                                      + "\n\tcon el resultado: "+ resultado);
                break;
            case 1:
                //Resta
                resultado = punto.getX() - punto.getY();
                calculo.setContenido("Se ha realizado la resta de " + punto
                                      + "\n\tcon el resultado: "+ resultado);
                break;
            case 2:
                //Multiplicación
                resultado = punto.getX() * punto.getY();
                calculo.setContenido("Se ha realizado la multiplicación de " + punto
                                      + "\n\tcon el resultado: "+ resultado);
                break;
            default:
                //División
                resultado = punto.getX() / punto.getY();
                calculo.setContenido("Se ha realizado la división de " + punto
                                          + "\n\tcon el resultado: "+ resultado);
                if( Double.isInfinite(resultado) || Double.isNaN(resultado) ) {
                    throw new ArithmeticException("División por 0");
                }
        }
        
        return calculo;
    }
}
