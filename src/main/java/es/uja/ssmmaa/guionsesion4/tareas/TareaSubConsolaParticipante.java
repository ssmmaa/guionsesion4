/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion4.tareas;

import es.uja.ssmmaa.guionsesion4.ontologia.Vocabulario.Incidencia;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.MensajeConsola;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.Respuesta;
import es.uja.ssmmaa.guionsesion4.ontologia.elm.SolicitarSubscripcion;
import es.uja.ssmmaa.guionsesion4.util.GestorSubscripciones;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SubscriptionResponder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Protocolo Subcribe para el agente con el rol participante. Responderá a las
 * solicitudes de subscripción y generarán el objeto Subsctiption asociado para
 * almacenarlo en el gestor de susbscripciones y así poder enviarle los MensajeConsola
 * cuando estén disponibles en el agente.
 * @author pedroj
 */
public class TareaSubConsolaParticipante extends SubscriptionResponder {
    private Subscription suscripcionJugador;
    private EnvioConsola<MensajeConsola> agente;
    private Respuesta infMensaje;
    private GestorSubscripciones gestor;
    
    // Para que la tarea pueda trabajar con la ontología del agente
    private final ContentManager manager;
    private final Ontology ontology;
    
    public TareaSubConsolaParticipante(Agent a, MessageTemplate mt, SubscriptionManager sm) {
        super(a, mt, sm);
        
        this.agente = (EnvioConsola) a;
        this.infMensaje = new Respuesta(this.agente.getServicio());
        this.gestor = agente.getGestor();
        
        this.manager = this.agente.getManager();
        this.ontology = this.agente.getOntology();
    }

    /**
     * Se cancelará la subscripión activa de un agente atendiendo a su soliciud.
     * Si hay algún problema en el objeto Subscription asociado se comunicará que
     * ha fallado la cancelación.
     * @param cancel
     *          Mensaje ACL de cancelación.
     * @return
     *          Mensaje ACL informando sobre la cancelación.
     * @throws FailureException 
     *          Mensaje ACL si se ha producido algún problema en la cancelación.
     */
    @Override
    protected ACLMessage handleCancel(ACLMessage cancel) throws FailureException {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        
        // Eliminamos la suscripción del agente jugador
        String nombreAgente = cancel.getSender().getName();
        
        // Mensaje de cancelación
        ACLMessage msg = cancel.createReply();
        msg.setPerformative(ACLMessage.INFORM);
                
        try {
            suscripcionJugador = gestor.getSubscripcion(nombreAgente);
            mySubscriptionManager.deregister(suscripcionJugador);
        } catch ( Exception e ) {
            msg.setPerformative(ACLMessage.FAILURE);
            infMensaje.setJustificacion(Incidencia.ERROR_CANCELACION);
            System.out.println(myAgent.getLocalName() + "\n\t" + infMensaje);
        }
        
        if( msg.getPerformative() != ACLMessage.FAILURE ) {
            infMensaje.setJustificacion(Incidencia.SUBSCRIPCION_CANCELADA);
            msgConsola.setContenido("Subscripción cancelada para " + nombreAgente);
        }
        
        try {
            manager.fillContent(msg, infMensaje);
        } catch (Codec.CodecException | OntologyException ex) {
            Logger.getLogger(TareaSubConsolaParticipante.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        agente.setMensaje(msgConsola);
        return msg;
    }

    /**
     * Se aceptará la subscripció si no se produce ningún problema en la generación
     * del objeto Subscription asociado al agente que la solicita.
     * @param subscription
     *          Mensaje ACL con la solicitud de subscripción.
     * @return
     *          Mensaje ACL con la aceptación de la subscripción.
     * @throws NotUnderstoodException
     *          No se trata por no analizar el contenido del mensaje subscription
     * @throws RefuseException 
     *          Mensaje ACL con el rechazo si no se ha podico completar la subscripción.
     */
    @Override
    protected ACLMessage handleSubscription(ACLMessage subscription) throws NotUnderstoodException, 
                                                                                RefuseException {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        String nombreAgente = null;
        
        // Creamos la respuesta a la subscripción
        ACLMessage msg = subscription.createReply();
        msg.setPerformative(ACLMessage.AGREE);
        
        if ( !subscription.getOntology().equals(ontology.getName()) ) {
            msg.setPerformative(ACLMessage.NOT_UNDERSTOOD);
            infMensaje.setJustificacion(Incidencia.ONTOLOGIA_DESCONOCIDA);
            msgConsola.setContenido(infMensaje.toString());
        } else {
            try {
                Action ac = (Action) manager.extractContent(subscription);
                SolicitarSubscripcion sub = (SolicitarSubscripcion) ac.getAction();
                nombreAgente = sub.getAgente().getName();
            
                // Registra la suscripción si no hay una previa
                if (!gestor.haySubscripcion(nombreAgente)) {
                    suscripcionJugador = createSubscription(subscription);
                    mySubscriptionManager.register(suscripcionJugador);
                }   
            } catch (Exception e) {
                msg.setPerformative(ACLMessage.REFUSE);
                infMensaje.setJustificacion(Incidencia.ERROR_REGISTRO_SUBSCRIPCION);
                System.out.println(myAgent.getLocalName() + "\n\t" + infMensaje);
            }
        }    
        
        if( msg.getPerformative() == ACLMessage.AGREE ) {
            // Responde afirmativamente a la suscripción
            infMensaje.setJustificacion(Incidencia.SUBSCRIPCION_CREADA);
            msgConsola.setContenido("Subscripción creada para " + nombreAgente);
        }
        
        // Completamos el contenido del mensaje
        try {
            manager.fillContent(msg, infMensaje);
        } catch (Codec.CodecException | OntologyException ex) {
            Logger.getLogger(TareaSubConsolaParticipante.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        agente.setMensaje(msgConsola);
        return msg;
    }
}